const http = require('http');
const commandLineArgs = require('command-line-args');

let requestCount = 0;

const server = http.createServer((req, res) => {
  requestCount += 1;
  res.write(JSON.stringify({ message: 'Request handled successfully', requestCount }));

  return res.end();
});

const optionsDefinitions = [{ name: 'port', alias: 'p', type: Number }];

const options = commandLineArgs(optionsDefinitions);

const PORT = options.port || 3000;

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
