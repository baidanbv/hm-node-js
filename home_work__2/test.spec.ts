import { Card, Transaction, CurrencyEnum } from './index';
import { beforeEach, describe, expect, test } from '@jest/globals';

describe('Card class', () => {
  let card;

  beforeEach(() => {
    card = new Card();
  });

  test('Adding a transaction', () => {
    const transaction = new Transaction(CurrencyEnum.UAH, 10);
    const id = card.addTransaction(transaction);
    expect(card.transactions).toHaveLength(1);
    expect(id).toBeTruthy();
  });

  test('Getting a transaction', () => {
    const transaction = new Transaction(CurrencyEnum.UAH, 10);
    card.addTransaction(transaction);
    const retrievedTransaction = card.getTransaction(transaction.id);
    expect(retrievedTransaction).toEqual(transaction);
  });

  test('Getting balance in USD', () => {
    const transaction1 = new Transaction(CurrencyEnum.USD, 10);
    const transaction2 = new Transaction(CurrencyEnum.USD, 20);
    card.addTransaction(transaction1);
    card.addTransaction(transaction2);
    expect(card.getBalance(CurrencyEnum.USD)).toBe(30);
  });

  test('Getting balance in UAH', () => {
    const transaction1 = new Transaction(CurrencyEnum.UAH, 50);
    const transaction2 = new Transaction(CurrencyEnum.UAH, 100);
    card.addTransaction(transaction1);
    card.addTransaction(transaction2);
    expect(card.getBalance(CurrencyEnum.UAH)).toBe(150);
  });
});