import { v4 as uuidv4 } from 'uuid';

export enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH'
}

export class Transaction {
  id: string = uuidv4();
  amount: number;
  currency: CurrencyEnum;

  constructor(currency: CurrencyEnum, amount: number) {
    this.currency = currency;
    this.amount = amount;
  }
}

export class Card {
  transactions: Transaction[];

  constructor() {
    this.transactions = [];
  }

  addTransaction(value: Transaction): string;
  addTransaction(value1: CurrencyEnum, value2: number): string;

  addTransaction(value1: CurrencyEnum | Transaction, value2?: number): string {
    let transaction: Transaction;

    if (value1 instanceof Transaction) {
      transaction = value1;
    } else if (Object.values(CurrencyEnum).includes(value1) && typeof value2 === 'number') {
      transaction = new Transaction(value1, value2);
    } else {
      console.log('Invalid data');

      return '';
    }

    this.transactions.push(transaction);

    return transaction.id;
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find((item) => item.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions.filter((item) => item.currency === currency).reduce((acc, current) => acc + current.amount, 0);
  }
}


const transaction1 = new Transaction(CurrencyEnum.UAH, 2);
const transaction2 = new Transaction(CurrencyEnum.UAH, 2);
const transaction3 = new Transaction(CurrencyEnum.USD, 3);
const transaction4 = new Transaction(CurrencyEnum.USD, 2);

const card = new Card();

card.addTransaction(transaction1);
card.addTransaction(transaction2);
card.addTransaction(transaction3);
card.addTransaction(transaction4);

card.addTransaction(CurrencyEnum.USD, 45);

console.log('Transactions Array: ', card.transactions);
console.log('Get Transaction: ', card.getTransaction(transaction2.id));
console.log('Get Balance: ', card.getBalance(CurrencyEnum.USD));
console.log('Get Balance: ', card.getBalance(CurrencyEnum.UAH));




