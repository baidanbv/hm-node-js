import knex from 'knex';
import path from 'path';

class DatabaseService {
  private static instance: DatabaseService;
  private db: any;

  constructor() {
    this.db = knex({
      client: 'sqlite3',
      connection: {
        filename: path.join(__dirname, '../db.sqlite')
      },
      useNullAsDefault: true
    });
  }

  public static getInstance(): DatabaseService {
    if (!DatabaseService.instance) {
      DatabaseService.instance = new DatabaseService();
    }
    return DatabaseService.instance;
  }

  async createTables() {
    const hasUsersTable = await this.db.schema.hasTable('users');

    if (hasUsersTable) {
      return;
    }

    await this.db.schema.createTable('users', (table: any) => {
      table.increments('id').primary();
      table.string('name');
      table.string('email');
      table.string('phone');
    });
  }

  async create(table: string, data: any) {
    return this.db
      .insert(data)
      .into(table)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  async readAllUsers() {
    return this.db.select().from('users').returning('*');
  }

  async read(table: string, id: number) {
    return this.db
      .select()
      .from(table)
      .where('id', id)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  async update(table: string, id: number, newData: any) {
    return this.db(table).where('id', id).update(newData);
  }

  async delete(table: string, id: number) {
    return this.db(table).where('id', id).del();
  }
}

export default DatabaseService.getInstance();
