import express, { Application } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { MainController } from './controllers'
import Routes from './routes'
import { DatabaseService } from './services'

class App {
    private app: Application
    private mainController: MainController

    constructor() {
        this.app = express()
        this.mainController = new MainController()
    }

    routing() {
        this.app.get('/', this.mainController.getStartPage)

        Object.keys(Routes).forEach((key) => {
            this.app.use(`/${key}`,  Routes[key])
        })
    }

    initPlugins() {
        this.app.use(bodyParser.json())
        this.app.use(morgan('dev'))
    }
    
    async start() {
        await DatabaseService.createTables()

        this.initPlugins()
        this.routing()

        this.app.listen(3000, () => {
            console.log('Server is running on port 3000')
        })
    }
}

const app = new App()
app.start()


