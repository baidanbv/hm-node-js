import { Router } from 'express';
import { NewsController } from '../controllers';

class NewsRouter {
    private router: Router;
    private newsController: NewsController;

    constructor() {
        this.router = Router();
        this.newsController = new NewsController();
        this.routes();
    }

    routes() {
        this.router.post('/', this.newsController.create);
        this.router.get('/', this.newsController.read);
        this.router.put('/', this.newsController.update);
        this.router.delete('/', this.newsController.delete);
    }

    getRouter() {
        return this.router;
    }
}

const newsRouter = new NewsRouter();

export default newsRouter.getRouter();