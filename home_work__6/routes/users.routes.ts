import { Router } from 'express';
import { UserController } from '../controllers';

class UserRouter {
  private router: Router;
  private userController: UserController;

  constructor() {
    this.router = Router();
    this.userController = new UserController();

    this.routes();
  }

  routes() {
    this.router.route('/').post(this.userController.create).get(this.userController.readAllUsers);
    this.router.route('/:id').get(this.userController.readSingleUser).put(this.userController.update).delete(this.userController.delete);
  }

  getRouter() {
    return this.router;
  }
}

const userRouter = new UserRouter();

export default userRouter.getRouter();
