import { Router } from 'express';
import UsersRouter from './users.routes';
import NewsRouter from './news.routes';

interface RoutesInterface {
    [index: string]: Router
  }

const Routes: RoutesInterface = {
  user: UsersRouter,
  news: NewsRouter,
}

export default Routes;