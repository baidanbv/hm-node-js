import { Request, Response } from 'express';
import { DatabaseService } from '../services';

export class UserController {
  async create(req: Request, res: Response) {
    try {
      const user = await DatabaseService.create('users', req.body);

      res.status(201);
      res.json({ message: 'User created', user });
    } catch (error) {
      res.status(500);
      res.json({ message: 'Error creating user' });
    }
  }
  async readAllUsers(req: Request, res: Response) {
    try {
      const users = await DatabaseService.readAllUsers();
      res.status(200);
      res.json({ users });
    } catch (error) {
      res.status(404);
      res.json({ message: 'Users not exists' });
    }
  }

  async readSingleUser(req: Request, res: Response) {
    try {
      const user = await DatabaseService.read('users', Number(req.params.id));
      res.status(200);
      res.json({ user, message: 'User read' });
    } catch (error) {
      res.status(404);
      res.json({ message: 'User not founded' });
    }
  }

  async update(req: Request, res: Response) {
    try {
      await DatabaseService.update('users', Number(req.params.id), req.body);
      res.status(200);
      res.json({ updatedData: req.body, message: 'User updated' });
    } catch (error) {
      res.status(500);
      throw new Error('Error updating user');
    }
  }

  async delete(req: Request, res: Response) {
    try {
      await DatabaseService.delete('users', Number(req.params.id));
      res.status(200);
      res.json({ message: 'User was  removed' });
    } catch (error) {
      res.status(500);
      throw new Error('Error updating user');
    }
  }
}
