import { Request, Response } from 'express'

export class NewsController {
    create(req: Request, res: Response) {
        res.status(201)
        res.json({ message: 'News created' })
    }

    read(req: Request, res: Response) {
        res.status(200)
        res.json({ message: 'News read' })
    }

    update(req: Request, res: Response) {
        res.status(200)
        res.json({ message: 'News updated' })
    }

    delete(req: Request, res: Response) {
        res.status(200)
        res.json({ message: 'News deleted' })
    }
}