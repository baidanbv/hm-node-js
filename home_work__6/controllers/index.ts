import { MainController } from './main.controller';
import { UserController } from './user.controller';
import { NewsController } from './news.controller';

export {
    MainController,
    UserController,
    NewsController,
}