import axios from 'axios';

const main = async () => {
    const URL = 'http://localhost:3000';

    // const response = await axios.get(URL);

    // console.log("main", response.data);


    const createUser = await axios.post(`${URL}/user?name=StringMan&age=30`, { name: "ObjectMan", email: "test@test.com", phone: "+9999" });
    // console.log("createUser", createUser);
    console.log("createUser", createUser.data);


    const getUser = await axios.get(`${URL}/user/${createUser.data.user.id}`);
    console.log("getUser", getUser.data);

    // const updateUser = await axios.put(`${URL}/user`);
    // console.log("updateUser", updateUser.data);

    // const deleteUser = await axios.delete(`${URL}/user`);
    // console.log("deleteUser", deleteUser.data);

    // const getNews = await axios.get(`${URL}/news`);
    // console.log("getNews", getNews.data);

    // const createNews = await axios.post(`${URL}/news`);
    // console.log("createNews", createNews.data);

    // const updateNews = await axios.put(`${URL}/news`);
    // console.log("updateNews", updateNews.data);

    // const deleteNews = await axios.delete(`${URL}/news`);
    // console.log("deleteNews", deleteNews.data);
}


main()