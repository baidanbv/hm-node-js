import { v4 as uuidv4 } from 'uuid';

export enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH'
}

interface ICard {
  addTransaction(value: Transaction): string;
  addTransaction(value1: CurrencyEnum, value2: number): string;
  getTransaction(id: string): Transaction | undefined;
  getBalance(currency: CurrencyEnum): number;
}

export class Transaction {
  id: string = uuidv4();
  amount: number;
  currency: CurrencyEnum;

  constructor(currency: CurrencyEnum, amount: number) {
    this.currency = currency;
    this.amount = amount;
  }
}

export class Card implements ICard {
  transactions: Transaction[];

  constructor() {
    this.transactions = [];
  }

  addTransaction(value: Transaction): string;
  addTransaction(value1: CurrencyEnum, value2: number): string;

  addTransaction(value1: CurrencyEnum | Transaction, value2?: number): string {
    let transaction: Transaction;

    if (value1 instanceof Transaction) {
      transaction = value1;
    } else if (Object.values(CurrencyEnum).includes(value1) && typeof value2 === 'number') {
      transaction = new Transaction(value1, value2);
    } else {
      console.log('Invalid data');

      return '';
    }

    this.transactions.push(transaction);

    return transaction.id;
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find((item) => item.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions.filter((item) => item.currency === currency).reduce((acc, current) => acc + current.amount, 0);
  }
}

export class BonusCard extends Card {
  addTransaction(value1: CurrencyEnum | Transaction, value2?: number): string {
    let transaction: Transaction;

    if (value1 instanceof Transaction) {
      const bonusAmount = value1.amount * 0.1;
      const bonusTransaction = { id: uuidv4(), amount: bonusAmount, currency: value1.currency};
      this.transactions.push(bonusTransaction);
      transaction = value1;
    } else if (Object.values(CurrencyEnum).includes(value1) && typeof value2 === 'number') {
      const bonusAmount = value2 * 0.1;
      transaction = new Transaction(value1, value2);
      const bonusTransaction = new Transaction(value1, bonusAmount);
      this.transactions.push(bonusTransaction);
    } else {
      console.log('Invalid data');

      return '';
    }

    this.transactions.push(transaction);

    return transaction.id;
  }
}

export class Pocket {
  cards: { [name: string]: ICard };

  constructor() {
    this.cards = {};
  }

  addCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  removeCard(name: string): void {
    delete this.cards[name];
  }

  getCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  getTotalAmount(currency: CurrencyEnum): number {
    let totalAmount = 0;
    for (const cardName in this.cards) {
      if (this.cards.hasOwnProperty(cardName)) {
        totalAmount += this.cards[cardName].getBalance(currency);
      }
    }
    return totalAmount;
  }
}

const transaction1 = new Transaction(CurrencyEnum.UAH, 20);
const transaction2 = new Transaction(CurrencyEnum.USD, 30);

const pocket = new Pocket();
const card = new Card();
const bonusCard = new BonusCard();

pocket.addCard('Card', card);
pocket.addCard('BonusCard', bonusCard);

 card.addTransaction(transaction1);
 bonusCard.addTransaction(transaction2);

console.log(" card => ", card);
console.log("bonus card => ",bonusCard);

console.log(pocket.getTotalAmount(CurrencyEnum.UAH))
console.log(pocket.getTotalAmount(CurrencyEnum.USD))