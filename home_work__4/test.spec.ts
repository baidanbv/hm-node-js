import { Pocket, Card, BonusCard, CurrencyEnum, Transaction } from './index';
import { beforeEach, describe, expect, test } from '@jest/globals';


describe('Pocket', () => {
  let pocket: Pocket;
  let card: Card;
  let bonusCard: BonusCard;

  beforeEach(() => {
    pocket = new Pocket();
    card = new Card();
    bonusCard = new BonusCard();
  });

  test('Adding and getting cards', () => {
    pocket.addCard('Card', card);
    pocket.addCard('BonusCard', bonusCard);

    expect(pocket.getCard('Card')).toBe(card);
    expect(pocket.getCard('BonusCard')).toBe(bonusCard);
  });

  test('Removing cards', () => {
    pocket.addCard('Card', card);
    pocket.addCard('BonusCard', bonusCard);

    pocket.removeCard('Card');
    expect(pocket.getCard('Card')).toBeUndefined();
  });

  test('Getting total amount', () => {
    const transaction1 = new Transaction(CurrencyEnum.UAH, 20);
    const transaction2 = new Transaction(CurrencyEnum.USD, 30);

    card.addTransaction(transaction1);
    bonusCard.addTransaction(transaction2);

    pocket.addCard('Card', card);
    pocket.addCard('BonusCard', bonusCard);

    expect(pocket.getTotalAmount(CurrencyEnum.UAH)).toBe(20);
    expect(pocket.getTotalAmount(CurrencyEnum.USD)).toBe(33);
  });
});
