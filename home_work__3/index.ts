import * as fs from 'fs/promises';
import * as path from 'path';

interface Schema {
  id: number;
  title: string;
  text: string;
  createDate: Date;
}

interface PostData {
  title: string;
  text: string;
}

const newsPostSchema: Schema = {
  id: 1,
  title: 'Some title',
  text: 'Some text',
  createDate: new Date()
};

const generateID = (data: Schema[]) => {
  let id = Math.floor(Math.random() * 1000);

  data.forEach((item: Schema) => {
    if (item.id <= id) {
      id = Math.floor(Math.random() * 1000);
    } else {
      return id;
    }
  });

  return id;
};

const data: PostData = {
  title: 'У зоопарку Чернігова лисичка народила лисеня',
  text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
};

class FileDB {
  database: Schema[];
  constructor() {
    this.database = [];
  }

  private getDatabase = async (fileName: string) => {
    const filePath: string = path.join(__dirname, `database/${fileName}.json`);
    try {
      const data = await fs.readFile(filePath, 'utf-8');

      return JSON.parse(data);
    } catch (error) {
      throw new Error('Table not found');
    }
  };

  private writeDatabase = async (fileName: string, data: Schema[]) => {
    await fs.writeFile(path.join(__dirname, `database/${fileName}.json`), JSON.stringify(data, null, 2), 'utf-8');
  };

  async registerSchema(tableName: string, schema: Schema): Promise<void> {
    try {
      await this.writeDatabase(tableName, this.database);
    } catch (error) {
      throw new Error('Table already exists');
    }
  }

  async getTable(tableName: string) {
    return {
      getAll: async () => {
        return this.getDatabase(tableName);
      },
      getById: async (id: number) => {
        try {
          const data = await this.getDatabase(tableName);
          const post = data.find((post: Schema) => post.id === id);

          return post;
        } catch (error) {
          throw new Error(`Post by ${id}, not found`);
        }
      },
      create: async (postData: PostData) => {
        try {
          const newId = generateID(this.database);
          
          const data = await this.getDatabase(tableName);        
          const newPost: Schema = {
            id: newId,
            title: postData.title,
            text: postData.text,
            createDate: new Date()
          };
          data.push(newPost);
          await this.writeDatabase(tableName, data);

          return newPost;
        } catch (error) {
          throw new Error('Post not created');
        }
      },
      update: async (id: number, updateData: { title?: string; text?: string }) => {
        try {
          const data = await this.getDatabase(tableName);
          const toUpdate = data.find((item: Schema) => item.id === id);

          if (updateData.title) {
            toUpdate.title = updateData.title;
          }
          if (updateData.text) {
            toUpdate.text = updateData.text;
          }

          await this.writeDatabase(tableName, data);

          return toUpdate.id;
        } catch (error) {
          throw new Error(`Post by ${id}, was not updated`);
        }
      },
      delete: async (id: number) => {
        try {
          const data = await this.getDatabase(tableName);
          const toDelete = data.find((item: Schema) => item.id === id);
          const updatedData = data.filter((item: Schema) => item.id !== id);

          await this.writeDatabase(tableName, updatedData);

          return toDelete.id;
        } catch (error) {
          throw new Error(`Post by ${id}, was not deleted`);
        }
      }
    };
  }
}

const fileDB = new FileDB();

fileDB.registerSchema('newspost', newsPostSchema);

(async () => {
  const newspostTable = await fileDB.getTable('newspost');
  // console.log('newspostTable: ', newspostTable);

  const createdNewspost = await newspostTable.create(data);
  // console.log('createdNewspost: ', createdNewspost);

  // const newsposts = await newspostTable.getAll();
  // console.log('all posts: ', newsposts);

  // const newspost = await newspostTable.getById(1);
  // console.log('post by id: ', newspost);

  // const updatedNewsposts = await newspostTable.update(1, { title: 'Маленька лисичка' });
  // console.log('updatedNewsPost: ', updatedNewsposts);

  // const deletedId = await newspostTable.delete(1);
  // console.log('deletedId: ', deletedId);
})();
